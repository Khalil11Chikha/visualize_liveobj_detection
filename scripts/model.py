import numpy as np
import torch
import copy
import os
import collections

import torchvision.transforms as transforms

from utils.general import non_max_suppression, scale_coords
from scripts.metrics.mean_avg_precision import mean_average_precision


class CNNWrapper:

    def __init__(self, model, device, models_name, labels):
        self.model = model
        self.model.to(device)
        self.model.eval()
        self.models_name = models_name
        self.labels = labels

        # define the torchvision image transforms
        self.transform = transforms.Compose([
            transforms.ToTensor(),
        ])

    def predict_yolov5(self,
                       batch,
                       detection_threshold,
                       image_size,
                       pred_bboxes, pred_classes_ids, pred_labels, pred_scores):

        outputs = self.model(batch)  # get the predictions on the image
        predictions = non_max_suppression(outputs, conf_thres=detection_threshold, iou_thres=0.5,
                                          classes=None, agnostic=False,
                                          multi_label=False,
                                          labels=(),
                                          max_det=100)

        for det in predictions:
            if det is not None:  # is not None
                det[:, :4] = scale_coords((image_size[0], image_size[1]), det[:, :4],
                                          (image_size[0], image_size[1])).round()

                # Write results
                temp_pred_classes_ids = list()
                temp_pred_labels = list()
                temp_pred_scores = list()
                temp_pred_bboxes = list()
                for *xyxy, conf, cls in reversed(det):
                    temp_pred_classes_ids.append(self.labels.index(self.model.names[int(cls)]))
                    temp_pred_labels.append(self.model.names[int(cls)])
                    temp_pred_scores.append(conf.detach().cpu().tolist())
                    temp_pred_bboxes.append([xy.detach().cpu().tolist() for xy in xyxy])
                pred_classes_ids.append(temp_pred_classes_ids)
                pred_labels.append(temp_pred_labels)
                pred_scores.append(temp_pred_scores)
                pred_bboxes.append(temp_pred_bboxes)

    def predict(self,
                batch,
                detection_threshold,
                pred_bboxes, pred_classes_ids, pred_labels, pred_scores):

        outputs = self.model(batch)  # get the predictions on the image

        for output in outputs:
            scores = output['scores'].detach().cpu().numpy()
            classes_ids = output['labels'].detach().cpu().numpy()
            boxes = output['boxes'].detach().cpu().numpy()
            indices = np.where(scores > detection_threshold)

            # get boxes above the threshold score
            pred_labels.append(classes_ids[indices])  # list of numpy arrays
            pred_scores.append(scores[indices])  # list of numpy arrays
            pred_bboxes.append(boxes[indices])  # list of numpy arrays
            idx = self.labels
            pred_classes_ids.append(np.asarray(idx)[classes_ids[indices]])

    def predict_and_calculate_mean_average_precision(self, path, models_name, iou_thresholds):
        # https://pytorch.org/vision/stable/models.html#object-detection-instance-segmentation-and-person-keypoint-detection
        # fasterrcnn_resnet50_fpn - 37.0
        # ssdlite320_mobilenet_v3_large - 21.3
        # https://jonathan-hui.medium.com/map-mean-average-precision-for -object - detection - 45c121a31173

        text_file = open(os.path.join(path, "mAP_output.txt"), "a")

        def xywh2xyxy(x):
            # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
            y = copy.deepcopy(x)
            y[0] = x[0]  # - x[:, 2] / 2  # top left x
            y[1] = x[1]  # - x[:, 3] / 2  # top left y
            y[2] = x[0] + x[2]  # bottom right x
            y[3] = x[1] + x[3]  # bottom right y
            return y

        def nested_dict():
            return collections.defaultdict(nested_dict)

        dict_average_precisions = nested_dict()
        list_of_average_precisions = list()
        nan_classes = [0, 12, 26, 29, 30, 45, 66, 68, 69, 71, 83]
        # COCO AP - AP @ [.50: .05:.95]:
        list_of_true_boxes = list()
        list_of_predictions = list()

        with torch.no_grad():
            for counter, (image_indices, image, gts) in enumerate(self.dataset.dataset_iterator()):
                pred_bboxes, pred_labels, pred_classes_ids, pred_scores = list(), list(), list(), list()
                if self.models_name == "yolov5":
                    image_sizes = [image_shape.shape for image_shape in image.data]
                    self.predict_yolov5(image, 0.5, (image_sizes[0][1], image_sizes[0][2]), pred_bboxes, pred_labels,
                                        pred_classes_ids, pred_scores)
                else:
                    self.predict(image, 0.5, pred_bboxes, pred_labels, pred_classes_ids, pred_scores)

                for gt, pred_bb, pred_lab, pred_id, pred_score, index in \
                        zip(gts, pred_bboxes, pred_labels, pred_classes_ids, pred_scores, image_indices):
                    # Removing N/A and __background__ classes from calculation
                    for bb, lab, score in zip(pred_bb, pred_id, pred_score):
                        if lab not in nan_classes:
                            list_of_predictions.append([index, lab, score, bb])

                    for entry in gt:
                        if entry['category_id'] not in nan_classes:
                            true_boxes = xywh2xyxy(entry['bbox'])
                            list_of_true_boxes.append([index, entry['category_id'], 1.0, true_boxes])

        # with open(os.path.join(path, models_name + 'list_of_predictions.json'), 'w') as outfile:
        #    json.dump(list_of_predictions, outfile)

        # with open(os.path.join(path, models_name + 'list_of_true_boxes.json'), 'w') as outfile:
        #    json.dump(list_of_true_boxes, outfile)

        for iou_threshold in iou_thresholds:
            average_precision = mean_average_precision(list_of_predictions, list_of_true_boxes, dict_average_precisions,
                                                       iou_threshold=iou_threshold, box_format="corners",
                                                       num_classes=len(self.dataset.labels))
            average_precision_percentage = average_precision.item() * 100.0
            list_of_average_precisions.append(average_precision_percentage)
            text_file.write("AP {:.2f} for iou {}\n".format(average_precision_percentage, iou_threshold))

        average_precision = sum(list_of_average_precisions) / float(len(list_of_average_precisions))
        #print("COCO final AP Box for all images: {:.2f}".format(average_precision))
        text_file.write("COCO final AP Box for all images.\n")
        text_file.write("{:.2f} \n".format(average_precision))
        text_file.close()

        # with open(os.path.join(path, models_name + 'dict_average_precisions.json'), 'w') as outfile:
        #    json.dump(dict_average_precisions, outfile)

        # AP[0.5,0.95,0.05]
        ap_per_class_matrix = np.zeros(shape=(len(list(dict_average_precisions.keys())), len(iou_thresholds)),
                                       dtype=float)

        for index_label, (label, ious) in enumerate(dict_average_precisions.items()):
            for index_iou, (iou, ap) in enumerate(ious.items()):
                ap_per_class_matrix[index_label][index_iou] = ap

        return ap_per_class_matrix, average_precision, dict_average_precisions
