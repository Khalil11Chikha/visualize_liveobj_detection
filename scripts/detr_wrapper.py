# https://colab.research.google.com/github/facebookresearch/detr/blob/colab/notebooks/detr_demo.ipynb
import os
import copy
import collections
import json
import numpy as np

import torch
import torchvision.transforms as T

from utils.general import non_max_suppression, scale_coords
from scripts.metrics.mean_avg_precision import mean_average_precision

torch.set_grad_enabled(False)

# standard PyTorch mean-std input image normalization
transform = T.Compose([
    T.Resize(800),
    T.ToTensor(),
    T.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
])


# for output bounding box post-processing
def box_cxcywh_to_xyxy(x):
    x_c, y_c, w, h = x.unbind(1)
    b = [(x_c - 0.5 * w), (y_c - 0.5 * h),
         (x_c + 0.5 * w), (y_c + 0.5 * h)]
    return torch.stack(b, dim=1)


def rescale_bboxes(out_bbox, size):
    img_h, img_w = size
    b = box_cxcywh_to_xyxy(out_bbox)
    b = b * torch.cuda.FloatTensor([img_w, img_h, img_w, img_h])
    return b


class DETRWrapper:
    def __init__(self, model, dataset):
        self.model = model
        self.dataset = dataset

    def predict(self, batch, prediction_thr, pred_bboxes, pred_classes_ids, pred_labels, pred_scores):
        # mean-std normalize the input image (batch-size: 1)
        # img = transform(im).unsqueeze(0)

        # demo model only support by default images with aspect ratio between 0.5 and 2
        # if you want to use images with an aspect ratio outside this range
        # rescale your image so that the maximum size is at most 1333 for best results
        assert batch[0].shape[-2] <= 1600 and batch[0].shape[
            -1] <= 1600, 'demo model only supports images up to 1600 pixels on each side'

        # propagate through the model
        outputs = self.model(batch)

        pred_logits = outputs['pred_logits'].softmax(-1)[:, :, :-1]
        pred_boxes = outputs['pred_boxes']
        for probabilities, pred_boxes in zip(pred_logits, pred_boxes):
            # keep only predictions with 0.7+ confidence
            keep = probabilities.max(-1).values > prediction_thr

            # convert boxes from [0; 1] to image scales
            bboxes_scaled = rescale_bboxes(pred_boxes[keep], batch.shape[2:]).detach().cpu().tolist()
            classes_ids = probabilities[keep].argmax(axis=-1).detach().cpu().tolist()

            if len(probabilities[keep].detach().cpu().tolist()) == 0:
                pred_scores.append(probabilities[keep].detach().cpu().tolist())
            else:
                pred_scores.append(probabilities[keep].detach().cpu().tolist()[0])
            pred_bboxes.append(bboxes_scaled)
            pred_classes_ids.append(classes_ids)
            pred_labels.append([self.dataset.labels[classes_ids[i]] for i in range(len(classes_ids))])


    def predict_and_calculate_mean_average_precision(self, path, models_name, iou_thresholds):
        # https://pytorch.org/vision/stable/models.html#object-detection-instance-segmentation-and-person-keypoint-detection
        # fasterrcnn_resnet50_fpn - 37.0
        # ssdlite320_mobilenet_v3_large - 21.3
        # https://jonathan-hui.medium.com/map-mean-average-precision-for -object - detection - 45c121a31173

        text_file = open(os.path.join(path, "mAP_output.txt"), "w")

        def xywh2xyxy(x):
            # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
            y = copy.deepcopy(x)
            y[0] = x[0]  # - x[:, 2] / 2  # top left x
            y[1] = x[1]  # - x[:, 3] / 2  # top left y
            y[2] = x[0] + x[2]  # bottom right x
            y[3] = x[1] + x[3]  # bottom right y
            return y

        def nested_dict():
            return collections.defaultdict(nested_dict)

        dict_average_precisions = nested_dict()
        list_of_average_precisions = list()
        nan_classes = [0, 12, 26, 29, 30, 45, 66, 68, 69, 71, 83]
        # COCO AP - AP @ [.50: .05:.95]:
        list_of_true_boxes = list()
        list_of_predictions = list()

        with torch.no_grad():
            for counter, (image_indices, image, gts) in enumerate(self.dataset.dataset_iterator()):
                pred_bboxes, pred_labels, pred_classes_ids, pred_scores = list(), list(), list(), list()
                self.predict(image, 0.5, pred_bboxes, pred_classes_ids, pred_labels, pred_scores)

                for gt, pred_bb, pred_lab, pred_id, pred_score, index in \
                        zip(gts, pred_bboxes, pred_labels, pred_classes_ids, pred_scores, image_indices):
                    # Removing N/A and __background__ classes from calculation
                    for bb, lab, score in zip(pred_bb, pred_id, pred_score):
                        if pred_lab not in nan_classes:
                            list_of_predictions.append([index, lab, score, bb])

                    for entry in gt:
                        if entry['category_id'] not in nan_classes:
                            true_boxes = xywh2xyxy(entry['bbox'])
                            list_of_true_boxes.append([index, entry['category_id'], 1.0, true_boxes])

        #with open(os.path.join(path, models_name + 'list_of_predictions.json'), 'w') as outfile:
        #    json.dump(list_of_predictions, outfile)

        #with open(os.path.join(path, models_name + 'list_of_true_boxes.json'), 'w') as outfile:
        #    json.dump(list_of_true_boxes, outfile)

        for iou_threshold in iou_thresholds:
            average_precision = mean_average_precision(list_of_predictions, list_of_true_boxes, dict_average_precisions,
                                                       iou_threshold=iou_threshold, box_format="corners",
                                                       num_classes=len(self.dataset.labels))
            average_precision_percentage = average_precision.item() * 100.0
            list_of_average_precisions.append(average_precision_percentage)
            text_file.write("AP {:.2f} for iou {}\n".format(average_precision_percentage, iou_threshold))

        average_precision = sum(list_of_average_precisions) / float(len(list_of_average_precisions))
        print("COCO final AP Box for all images: {:.2f}".format(average_precision))
        text_file.write("COCO final AP Box for all images.\n")
        text_file.write("{:.2f} \n".format(average_precision))
        text_file.close()

        #with open(os.path.join(path, models_name + 'dict_average_precisions.json'), 'w') as outfile:
        #    json.dump(dict_average_precisions, outfile)

        # AP[0.5,0.95,0.05]
        ap_per_class_matrix = np.zeros(shape=(len(list(dict_average_precisions.keys())), len(iou_thresholds)), dtype=float)

        for index_label, (label, ious) in enumerate(dict_average_precisions.items()):
            for index_iou, (iou, ap) in enumerate(ious.items()):
                ap_per_class_matrix[index_label][index_iou] = ap

        return ap_per_class_matrix, average_precision, dict_average_precisions
