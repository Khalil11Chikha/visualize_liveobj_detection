import json
import numpy as np
import copy
import collections
import os
import torch
from tqdm import tqdm
import matplotlib.pyplot as plt

import torchvision.datasets as dset


# https://pytorch.org/vision/stable/_modules/torchvision/datasets/coco.html
def rescale(image, target, newsize):
    # For object detection annotations, the format is "bbox" : [x,y,width,height]
    # Where:
    # x, y: the upper-left coordinates of the bounding box
    # width, height: the dimensions of your bounding box
    #
    width, height = image.size

    scale_width = float(newsize[0]) / float(width)
    # newsize[1] = int(height * scale_width)
    scale_height = float(newsize[1]) / float(height)

    resized_image = image.resize(tuple(newsize))

    resized_target = copy.deepcopy(target)
    for det_object in resized_target:
        det_object["bbox"][0] = det_object["bbox"][0] * scale_width
        det_object["bbox"][1] = det_object["bbox"][1] * scale_height
        det_object["bbox"][2] = det_object["bbox"][2] * scale_width
        det_object["bbox"][3] = det_object["bbox"][3] * scale_height

    return resized_image, resized_target


class CustomDataSet:
    def __init__(self, path_to_data, path_to_labels, allowed_classes=None):
        self.num_classes, self.labels = self.open_labels(path_to_labels)
        self.labels_numpy = np.asarray(self.labels)
        self.color_maps = np.random.uniform(0, 255, size=(self.num_classes, 3))
        self.filter_labels = allowed_classes

        self.dataset = collections.defaultdict(list)
        from PIL import Image
        for root, dirs, files in os.walk(path_to_data, topdown=False):
            image_files = [file for file in files if file.split(".")[1].lower() == "jpg"]
            for name in image_files:
                image = Image.open(os.path.join(root, name))
                json_name = name.split(".")[0] + ".json"
                json_file = open(os.path.join(root, json_name))
                annotation = json.load(json_file)
                self.dataset[name.split(".")[0]].append((image, annotation))
                # self.dataset[name].append(annotation)

    @staticmethod
    def open_labels(path_to_labels):
        extension = path_to_labels.split(".")[-1]
        assert extension != "json" or extension != "txt", "Unknown format of labels."

        if extension == "json":
            with open(path_to_labels) as jsonFile:
                labels = json.load(jsonFile)
                jsonFile.close()
        else:
            with open(path_to_labels) as f:
                labels = list()
                for line in f:
                    labels.append(line)

        return len(labels), labels

    def dataset_iterator(self):
        for key, entry in self.dataset.items():
            image = entry[0][0]
            target = entry[0][1][key]
            yield image, target


class CocoDataSet:
    def __init__(self, path_to_data,
                 path_to_annotations,
                 path_to_labels,
                 allowed_classes=None,
                 device="cpu",
                 batch_size=16,
                 h_size=(800, 600),
                 v_size=(800, 1052),
                 images_to_be_investigated=["all"]):

        self.num_classes, self.labels = self.open_labels(path_to_labels)
        self.labels_numpy = np.asarray(self.labels)
        self.color_maps = np.random.uniform(0, 255, size=(self.num_classes, 3))
        self.filter_labels = allowed_classes
        if allowed_classes is None:
            self.filter_labels = self.labels
        self.device = device
        self.batch_size = batch_size
        self.h_size = h_size
        self.v_size = v_size
        self.images_to_be_investigated = images_to_be_investigated

        self.coco_val = dset.CocoDetection(root=path_to_data,
                                           annFile=path_to_annotations)

    @staticmethod
    def open_labels(path_to_labels):
        extension = path_to_labels.split(".")[-1]
        assert extension != "json" or extension != "txt", "Unknown format of labels."

        if extension == "json":
            with open(path_to_labels) as jsonFile:
                labels = json.load(jsonFile)
                jsonFile.close()
        else:
            with open(path_to_labels) as f:
                labels = list()
                for line in f:
                    labels.append(line.replace("\n", ""))

        return len(labels), labels

    @staticmethod
    def resize_image(image, target, size, batch_images, batch_targets, indices, index_batch, index, resize=True):
        resized_image, resized_target = rescale(image, target, size)
        image_numpy = np.asarray(resized_image, dtype=float)
        normalized = image_numpy / 255.0  # (image_numpy - np.min(image_numpy)) / (np.max(image_numpy) - np.min(image_numpy))
        batch_images.append(np.moveaxis(normalized, -1, 0))
        batch_targets.append(resized_target)
        indices.append(index)
        index_batch += 1
        return index_batch

    def dataset_iterator(self):
        index_v = 0
        index_h = 0
        batch_images_v = list()
        batch_images_h = list()
        batch_targets_v = list()
        batch_targets_h = list()
        indices_h = list()
        indices_v = list()

        for index, (image, target) in enumerate(self.coco_val):

            if index_h == self.batch_size:
                yield indices_h, torch.Tensor(np.asarray(batch_images_h)).to(self.device), batch_targets_h
                batch_images_h = list()
                batch_targets_h = list()
                indices_h = list()
                index_h = 0

            if index_v == self.batch_size:
                yield indices_v, torch.Tensor(np.asarray(batch_images_v)).to(self.device), batch_targets_v
                batch_images_v = list()
                batch_targets_v = list()
                indices_v = list()
                index_v = 0

            labels_list = [self.labels[entry['category_id']] for entry in target]
            if len(set(self.filter_labels) & set(labels_list)) > 0:
                width, height = image.size
                if self.images_to_be_investigated[0] == "all":
                    if width > height:
                        index_h = self.resize_image(image, target, self.h_size, batch_images_h, batch_targets_h,
                                                    indices_h, index_h, index)
                    else:
                        index_v = self.resize_image(image, target, self.v_size, batch_images_v, batch_targets_v,
                                                    indices_v, index_v, index)
                elif index in self.images_to_be_investigated:
                    if width > height:
                        index_h = self.resize_image(image, target, self.h_size, batch_images_h, batch_targets_h,
                                                    indices_h, index_h, index)
                    else:
                        index_v = self.resize_image(image, target, self.v_size, batch_images_v, batch_targets_v,
                                                    indices_v, index_v, index)


class SmallDataSet(CocoDataSet):

    def __init__(self, path_to_data,
                 path_to_annotations,
                 path_to_labels,
                 allowed_classes=None,
                 device="cpu",
                 batch_size=16,
                 h_size=(800, 600),
                 v_size=(800, 1052),
                 images_to_be_investigated=["all"]):

        CocoDataSet.__init__(self, path_to_data,
                             path_to_annotations,
                             path_to_labels,
                             allowed_classes=allowed_classes,
                             device=device,
                             batch_size=batch_size,
                             h_size=h_size,
                             v_size=v_size,
                             images_to_be_investigated=images_to_be_investigated)

        self.ground_truths = list()
        self.images = list()
        self.images_cnt = list()
        for image_indices, image, gts in self.prepare_dataset():
            self.ground_truths.append(gts)
            self.images.append(image)
            self.images_cnt.append(image_indices)

    def dataset_iterator(self):
        for indices, images, gts in zip(self.images_cnt, self.images, self.ground_truths):
            yield indices, images, gts

    def prepare_dataset(self):
        index_v = 0
        index_h = 0
        batch_images_v = list()
        batch_images_h = list()
        batch_targets_v = list()
        batch_targets_h = list()
        indices_h = list()
        indices_v = list()

        print("Preparing dataset: ")
        for index, (image, target) in tqdm(enumerate(self.coco_val)):

            if index_h == self.batch_size:
                yield indices_h, torch.Tensor(np.asarray(batch_images_h)).to(self.device), batch_targets_h
                batch_images_h = list()
                batch_targets_h = list()
                indices_h = list()
                index_h = 0

            if index_v == self.batch_size:
                yield indices_v, torch.Tensor(np.asarray(batch_images_v)).to(self.device), batch_targets_v
                batch_images_v = list()
                batch_targets_v = list()
                indices_v = list()
                index_v = 0

            labels_list = [self.labels[entry['category_id']] for entry in target]
            if len(set(self.filter_labels) & set(labels_list)) > 0:
                width, height = image.size
                if self.images_to_be_investigated[0] == "all":
                    if width > height:
                        index_h = self.resize_image(image, target, self.h_size, batch_images_h, batch_targets_h,
                                                    indices_h, index_h, index)
                    else:
                        index_v = self.resize_image(image, target, self.v_size, batch_images_v, batch_targets_v,
                                                    indices_v, index_v, index)
                elif index in self.images_to_be_investigated:
                    if width > height:
                        index_h = self.resize_image(image, target, self.h_size, batch_images_h, batch_targets_h,
                                                    indices_h, index_h, index)
                    else:
                        index_v = self.resize_image(image, target, self.v_size, batch_images_v, batch_targets_v,
                                                    indices_v, index_v, index)
                else:
                    break
