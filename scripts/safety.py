# from scripts.CNNWrapper import CNNWrapper

import collections
from collections import defaultdict
import copy
import json
import logging
import os
import numpy as np
import torch
from tqdm import tqdm


def calculate_criticality_confidence(
        conf: float,
        masked_conf: float,
        criticality_tau: float) -> float:
    criticality = conf - masked_conf

    return criticality


def calculate_criticality_detection(
        groundtruth_iou: float,
        masked_iou: float) -> float:
    # changed after discussion with Tobi - no more multiplied by 2
    criticality = groundtruth_iou - masked_iou

    return criticality


# class SafetyAlgorithm(CNNWrapper):
class SafetyAlgorithm:

    def __init__(self, MappedModel):
        super().__init__()

        self.MappedModel = MappedModel
        # self.original_layers = copy.deepcopy(self.MappedModel.renderable_layers)

        self.criticality_tau = 0.5

        log_file_dir = os.path.join('data', 'logging')
        if not os.path.exists(log_file_dir):
            os.makedirs(log_file_dir)

    @staticmethod
    def get_layers_dimension(_weights) -> list():
        if len(_weights.shape) == 4:
            # point-wise
            if _weights.shape[2] == 1 and _weights.shape[3] == 1:
                return [index for index in range(_weights.shape[0])]
            elif _weights.shape[1] == 1:
                return [index for index in range(_weights.shape[0])]
            # depth-wise
            else:
                return [index for index in range(_weights.shape[0])]

        elif len(_weights.shape) == 1:
            return [index for index in range(_weights.shape[0])]

        # to filter only dense layers
        elif len(_weights.shape) == 2:
            return [index for index in range(_weights.shape[0])]

        else:
            return [None]

    # @staticmethod
    # def unmask_kernel(_weights, _original_weights):
    #    _weights = _original_weights

    @staticmethod
    def unmask_kernel(_weights, _original_weights, _index: int):
        if len(_weights.shape) == 4:  # to filter only depth separable and conv layers
            _weights[_index, :, :, :] = copy.deepcopy(_original_weights[_index, :, :, :])

        # to filter only dense layers
        elif len(_weights.shape) == 2:
            _weights[_index, :] = copy.deepcopy(_original_weights[_index, :])

        elif len(_weights.shape) == 1:
            _weights[_index] = copy.deepcopy(_original_weights[_index])

    @staticmethod
    def multiple_mask_kernel(_weights, _indices: list):
        if len(_weights.shape) == 4:  # to filter only depth separable and conv layers
            for filters in _indices:
                _weights[filters, :, :, :] = 0.0

        # to filter only dense layers
        elif len(_weights.shape) == 2:
            for neurons in _indices:
                _weights[neurons, :] = 0.0

        elif len(_weights.shape) == 1:
            for neurons in _indices:
                _weights[neurons] = 0.0

    @staticmethod
    def mask_kernel(_weights, _index: int):
        # to filter only depth separable and conv layers
        if len(_weights.shape) == 4:
            _weights[_index, :, :, :] = 0.0

        # to filter only dense layers
        elif len(_weights.shape) == 2:
            _weights[_index, :] = 0.0

        elif len(_weights.shape) == 1:
            _weights[_index] = 0.0

    @staticmethod
    def calculate_criticality_adversary(des_adv_conf, new_conf, adv_ind, new_ind, ori_ind, adv_ori_conf,
                                        criticality_tau):

        # 1 case:
        """ if the new prediction has smaller confidence than original one """
        if new_ind != ori_ind and (des_adv_conf - adv_ori_conf) < criticality_tau:
            criticality = des_adv_conf - adv_ori_conf

        # 2 case:
        elif new_ind == ori_ind:
            if new_conf > 0.5:
                criticality = -2.0  # clip the criticality to 2
            else:
                criticality = -1.0 * (1 / (1 - (new_conf)))

        # 3 case:
        else:
            # Anti critical neurons, the criticality should be negative
            criticality = des_adv_conf - adv_ori_conf

        return criticality

    @staticmethod
    def calculate_criticality_classification(
            groundtruth_conf: list,
            masked_conf: list,
            goundtruth_index: list,
            masked_index: list,
            criticality_tau: float) -> list:

        criticality = list()
        for gt_conf, m_conf, gt_index, m_index in zip(groundtruth_conf,
                                                       masked_conf,
                                                       goundtruth_index,
                                                       masked_index):

            # 1 case:
            """ if the new prediction has smaller confidence than original one """
            if ((gt_conf - m_conf) > criticality_tau) and (gt_index == m_index):
                criticality.append(gt_conf - m_conf)

            # 2 case:
            elif gt_index != m_index:
                if m_conf > 0.5:
                    criticality.append(2.0)  # clip the criticality to 2
                else:
                    criticality.append(1 / (1 - m_conf))

            # 3 case:
            else:
                # Anti critical neurons, the criticality should be negative
                criticality.append(gt_conf - m_conf)

        return criticality

    @staticmethod
    def calculate_criticality_FP(
            num_fp: int,
            num_masked_fp: int) -> float:
        """
        Function to calculate the criticality of FP for a class in an image.
        :param num_fp: Number of FPs before masking
        :param num_masked_fp: Number of FPs after masking
        :return: criticality for the FP
        """
        epsilon = 1e-6

        if num_fp != 0 or num_masked_fp != 0:
            if (num_masked_fp - num_fp) / (num_fp + epsilon) < 2.0:
                criticality = (num_masked_fp - num_fp) / (num_fp + epsilon)
            else:
                criticality = 2.0
        else:
            criticality = 0.0

        return criticality

    @staticmethod
    def calculate_criticality_TP_FN(
            tp_array: [int],
            masked_tp_array: [int],
            tp_array_iou: [float],
            masked_tp_array_iou: [float],
            tp_array_conf: [float],
            masked_tp_array_conf: [float]) -> float:
        """
        Calculation of Criticality for all TP and FN of one class in an image.


        :param tp_array: Array of zeros and ones indicating which GT was detected.
        :param masked_tp_array: Array of zeros and ones indicating which GT was detected after masking.
        :param tp_array_iou: Array of IOUs of GTs. Zero if the GT was missed.
        :param masked_tp_array_iou: Array of IOUs of GTs after masking. Zero if the GT was missed.
        :param tp_array_conf: Array of confidence values of GTs. Zero if the GT was missed.
        :param masked_tp_array_conf: Array of confidence values of GTs after masking. Zero if the GT was missed.
        :return: criticality averaged over the GTs.
        """
        criticality = 0.0
        criticality_tau = 0.0

        if len(tp_array) == 0:
            return 0.0

        # Loop through all GTs
        for tp_arr_conf, m_tp_arr_conf, tp_arr_iou, m_tp_arr_iou in zip(tp_array_conf,
                                                                        masked_tp_array_conf,
                                                                        tp_array_iou,
                                                                        masked_tp_array_iou):
            conf_cri = calculate_criticality_confidence(tp_arr_conf,
                                                        m_tp_arr_conf,
                                                        criticality_tau)

            det_cri = calculate_criticality_detection(tp_arr_iou,
                                                      m_tp_arr_iou)

            criticality += conf_cri + det_cri

        return criticality / len(tp_array)

    @staticmethod
    def calculate_resulting_criticality(cr_sum: dict,
                                        criticality_cc: dict,
                                        criticality_dc: dict):

        for key in criticality_cc:
            cr_sum[key] = criticality_cc[key] + criticality_dc[key]

    @staticmethod
    def nested_dict(depth=1):
        if depth == 1:
            return defaultdict(list)
        elif depth == 2:
            return defaultdict(lambda: defaultdict(list))
        elif depth == 3:
            return defaultdict(lambda: defaultdict(lambda: defaultdict(list)))
        else:
            return defaultdict(SafetyAlgorithm.nested_dict(4))

    def criticality_per_frame(self,
                              gt_classes: list,
                              tp_list: list,
                              masked_tp_list: list,
                              tp_iou_list: list,
                              masked_tp_iou_list: list,
                              tp_conf_list: list,
                              masked_tp_conf_list: list,
                              non_masked_fp: list,
                              masked_fp: list) -> [dict, dict, dict]:

        result_tp_fn = SafetyAlgorithm.nested_dict(depth=1)
        result_fp = SafetyAlgorithm.nested_dict(depth=1)
        cr_sum = SafetyAlgorithm.nested_dict(depth=1)

        for i, class_name in enumerate(self.DataSet.labels):

            if class_name not in gt_classes:
                continue

            indices_tp = np.where(np.asarray(gt_classes) == class_name)

            tps = tp_list[indices_tp]
            masked_tps = masked_tp_list[indices_tp]
            tps_iou = tp_iou_list[indices_tp]
            masked_tps_iou = masked_tp_iou_list[indices_tp]
            tps_conf = tp_conf_list[indices_tp]
            masked_tps_conf = masked_tp_conf_list[indices_tp]

            result_tp_fn[class_name] = self.calculate_criticality_TP_FN(
                tps,
                masked_tps,
                tps_iou,
                masked_tps_iou,
                tps_conf,
                masked_tps_conf)

            indices_masked_fp = np.where(masked_fp == i)
            indices_non_masked_fp = np.where(non_masked_fp == i)

            result_fp[class_name] = self.calculate_criticality_FP(
                np.sum(indices_non_masked_fp),
                np.sum(indices_masked_fp)
            )

        self.calculate_resulting_criticality(cr_sum, result_tp_fn, result_fp)

        return result_tp_fn, result_fp, cr_sum

    def get_conf_and_class(self, des_cls, input_batch, remove=False):

        # Get outputs from chosen layers and calculate maximum responses
        output = self.MappedModel(input_batch)
        probabilities = torch.nn.functional.softmax(output, dim=1)
        probabilities = probabilities.cpu().detach().numpy()

        pre_ind = np.argmax(probabilities, axis=1)
        pre_conf = np.max(probabilities, axis=1)
        pre_cls = [self.DataSet.list_of_classes_labels[index] for index in pre_ind]

        des_ind = [self.DataSet.list_of_classes_labels.index(index) for index in des_cls]
        des_conf = [probabilities[index][label_index] for index, label_index in enumerate(des_ind)]

        return pre_ind, pre_conf, des_ind, des_conf

    def analyse_classification_criticality_via_plain_masking(self,
                                                             device,
                                                             models_name):

        def nested_dict():
            return collections.defaultdict(nested_dict)

        statistics_path = os.path.join("data", models_name + "_statistics_dict.json")
        masked_layers = self.MappedModel.renderable_layers
        with torch.no_grad():

            for batch_name, batch, label in self.DataSet.dataset_iterator():
                batch_tensor = torch.FloatTensor(batch).to(device)
                pre_ind, pre_conf, des_ind, des_conf = self.get_conf_and_class(label, batch_tensor)

                for original_layers_name in masked_layers.keys():
                    logging.error("Processing layer: " + original_layers_name)
                    layer_stats_json = nested_dict()

                    weights = masked_layers[original_layers_name].weight.data
                    original_weights = copy.deepcopy(weights)
                    indices = self.get_layers_dimension(weights)
                    # print(weights.shape)

                    for each_filter in tqdm(range(indices)):
                        # mask the related neuron
                        self.mask_kernel(masked_layers[original_layers_name].weight.data, [each_filter])

                        output = self.MappedModel(batch_tensor)

                        probabilities = torch.nn.functional.softmax(output, dim=1)
                        probabilities = probabilities.cpu().detach().numpy()

                        new_ind = np.argmax(probabilities, axis=1)
                        new_conf = np.max(probabilities, axis=1)

                        criticality = self.calculate_criticality_classification(pre_conf,
                                                                                new_conf,
                                                                                pre_ind,
                                                                                new_ind,
                                                                                self.criticality_tau)

                        criticality_str = [str(cri) for cri in criticality]
                        layer_stats_json[label[0]][original_layers_name][str(each_filter)] = criticality_str

                        # have to return back the weights at each kernel weight masking
                        masked_layers[original_layers_name].weight.data = copy.deepcopy(original_weights)

                    with open(statistics_path, 'w') as fp:
                        json.dump(layer_stats_json, fp)
