import collections
import cv2
from scripts.safety import SafetyAlgorithm
import scripts.functions as functions
import settings
from TestVideo import PreprocessVideo
import torch
from PIL import Image
import numpy as np
import json
import os
from scripts.metrics.mean_avg_precision import mean_average_precision

# @FixMe os.path.join
video = os.path.join("c:/", "Users", "benchikha", "IMG_5676.mov")
# initialize class with the video to be processed
preprocess_vid = PreprocessVideo(video)
# directory where frames extracted from the video have to be saved
# @FixMe os.path.join

# use cuda device in case of availability
frames = os.path.join("c:/", "Users", "benchikha", "frames")
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
# check number of frames and frequency
fps, _ = preprocess_vid.get_num_frames()

# get frames from input video and save them to path
# preprocess_vid.get_all_frames(path_to_save_frame=frames + "image")


# open the label list and return labels
def open_labels(path_to_labels):
    extension = path_to_labels.split(".")[-1]
    assert extension != "json" or extension != "txt", "Unknown format of labels."

    if extension == "json":
        with open(path_to_labels) as jsonFile:
            labels = json.load(jsonFile)
            jsonFile.close()
    else:
        with open(path_to_labels) as f:
            labels = list()
            for line in f:
                labels.append(line.replace("\n", ""))

    return len(labels), labels


def nested_dict():
    return collections.defaultdict(nested_dict)

# @FixMe not sequently but parallel - two models live

list_of_models = settings.models_names

dict_average_precisions_m1 = nested_dict()
dict_average_precisions_m2 = nested_dict()

to_eval_list_m_1, to_eval_list_m_2, list_of_evaluated_m1, list_of_evaluated_m2 = list(), list(), list(), list()
predicted_boxes_m1, gdt_boxes_m1, predicted_boxes_m2, gdt_boxes_m2 = list(), list(), list(), list()
gtd_boxes = list()

len_labels, All_labels = open_labels('coco_labels.json')  
model_1, param_dict_m1 = settings.take_a_model(list_of_models[0], device, All_labels)
model_2, param_dict_m2 = settings.take_a_model(list_of_models[0], device, All_labels)

models_dict = {'model_1': {'model_name': list_of_models[0], 'list_to_eval': to_eval_list_m_1,
                           'list_evaluated': list_of_evaluated_m1, 'aver_precision': dict_average_precisions_m1,
                           'predicted_boxes': predicted_boxes_m1, 'model_param': param_dict_m1, 'model': model_1},
               'model_2': {'model_name': list_of_models[1], 'list_to_eval': to_eval_list_m_2,
                           'list_evaluated': list_of_evaluated_m2, 'aver_precision': dict_average_precisions_m2,
                           'predicted_boxes': predicted_boxes_m2, 'model_param': param_dict_m2, 'model': model_2}}

safety_instance_m1 = SafetyAlgorithm(model_1)
safety_instance_m2 = SafetyAlgorithm(model_2)

# @FixMe could you take also video stream as input?
read_from_vid_from_path = True
if read_from_vid_from_path == True:
    cap = cv2.VideoCapture(video)
    total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    index = 0
    fno = 0
    sample_rate = 1
else:
    cap = cv2.VideoCapture(0)

path_to_image = os.path.join("data", "annotated_images", "image")

with torch.no_grad():
    for fno in range(0, total_frames, sample_rate):
        #cap.set(cv2.CAP_PROP_POS_FRAMES, fno)
        _, image_ = cap.read()
        print(type(image_))
        # @FixMe end of file?
        # @FixMe could you take also video stream as input? webcam?
        # @FixMe fixed?
        im_1_orignal = np.asarray(image_)
        im_1 = np.moveaxis(np.asarray(image_) / 255.0, -1, 0)
        im_1 = np.expand_dims(im_1, axis=0)
        im_1 = torch.Tensor(np.asarray(im_1)).to(device)

        # @FixMe os.path.join
        image = preprocess_vid.read_json_frame(path_to_image + str(index+1) + ".json")

        boxes_m1, classes_m1, labels_1, scores_m1, gts = list(), list(), list(), list(), list()
        boxes_m2, classes_m2, labels_2, scores_m2 = list(), list(), list(), list()
        for entries in image["shapes"]:
            _label = entries["label"]
            points = entries["points"]
            gts.append([_label, points])
            _box = np.reshape(np.array(gts[0][1]), (1, np.product(np.array(gts[0][1]).shape))).tolist()
            gtd_boxes.append([index, All_labels.index(_label), 1.0, [_box[0][0], _box[0][1], _box[0][2], _box[0][3]]])

            if models_dict['model_1']['model_name'] == "yolov5":
                image_sizes = [image_shape.shape for image_shape in im_1.data]
                model_1.predict_yolov5(im_1, 0.5, (image_sizes[0][1], image_sizes[0][2]), boxes_m1, classes_m1, labels_1,
                                       scores_m1)

            else:
                model_1.predict(im_1, 0.5, boxes_m1, classes_m1, labels_1, scores_m1)

            if models_dict['model_2']['model_name'] == "yolov5":
                image_sizes = [image_shape.shape for image_shape in im_1.data]
                model_2.predict_yolov5(im_1, 0.5, (image_sizes[0][1], image_sizes[0][2]), boxes_m2, classes_m2, labels_2,
                                       scores_m2)

            else:
                model_2.predict(im_1, 0.5, boxes_m2, classes_m2, labels_2, scores_m2)

            for box, cls, label, score, gt in zip(boxes_m1, classes_m1, labels_1, scores_m1, gts):
                # TBD - change the function "draw_boxes"
                # @FixMe fixed?
                for bb, lab, score in zip(box, label, score):
                    models_dict['model_1']['predicted_boxes'].append([index, lab, score, bb])

                labeled_image_1 = functions.draw_box(box,
                                                     cls,
                                                     label,
                                                     im_1_orignal)

            for box, cls, label, score, gt in zip(boxes_m2, classes_m2, labels_2, scores_m2, gts):
                # TBD - change the function "draw_boxes"
                # @FixMe fixed?
                for bb, lab, score in zip(box, label, score):
                    models_dict['model_2']['predicted_boxes'].append([index, lab, score, bb])

                labeled_image_2 = functions.draw_box(box,
                                                     cls,
                                                     label,
                                                     im_1_orignal)

            m_ave_pre_m_1 = mean_average_precision(models_dict['model_1']['predicted_boxes'],
                                                   gtd_boxes, models_dict['model_1']['aver_precision'],
                                                   iou_threshold=0.5, box_format="corners",
                                                   num_classes=len_labels)

            models_dict['model_1']['list_to_eval'].append(labeled_image_1)
            models_dict['model_1']['list_evaluated'].append(m_ave_pre_m_1)

            print(models_dict['model_1']['list_to_eval'])
            m_1 = models_dict['model_1']['model_name']

            m_ave_pre_m_2 = mean_average_precision(models_dict['model_2']['predicted_boxes'],
                                                   gtd_boxes, models_dict['model_2']['aver_precision'],
                                                   iou_threshold=0.5, box_format="corners", num_classes=len_labels)

            models_dict['model_2']['list_to_eval'].append(labeled_image_2)
            models_dict['model_2']['list_evaluated'].append(m_ave_pre_m_2)

            m_2 = models_dict['model_2']['model_name']
            print(models_dict['model_1']['list_to_eval'])

            preprocess_vid.eval_obj_det_per_frame(labeled_image_1, labeled_image_2,
                                                  m_ave_pre_m_1, m_ave_pre_m_2, m_1, m_2)
            if cv2.waitKey(25) and 0xFF == ord('q'):
                break


# @FixMe os.path.join

#path_to_save_video = os.path.join("c:/", "Users", "benchikha", "vid_concat_eval_both_models_2.avi")

# evaluate the accuracy of the models
# @FixMe please make it possible to run parallel to the prediction
# @FixMe one frame, pne prediction, one evaluation
# @FixMe you can make two modes - online and offline

#outputs = preprocess_vid.eval_obj_det(models_dict['model_1']['list_to_eval'], models_dict['model_2']['list_to_eval'],
#                                      models_dict['model_1']['list_evaluated'], models_dict['model_2']['list_evaluated']
#                                      , m_1, m_2, fps, path_to_save_video)






