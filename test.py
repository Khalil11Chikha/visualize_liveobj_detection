import os
import cv2
from scripts.safety import SafetyAlgorithm
import scripts.functions as functions
import settings
from TestVideo import PreprocessVideo
import torch
from PIL import Image
import numpy as np
import json

# import the modules



# get the path or directory
folder_dir = "../frames/"

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
preprocess_vid = PreprocessVideo("../IMG_5676.mov")

if __name__ == '__main__':

    def open_labels(path_to_labels):
        extension = path_to_labels.split(".")[-1]
        assert extension != "json" or extension != "txt", "Unknown format of labels."

        if extension == "json":
            with open(path_to_labels) as jsonFile:
                labels = json.load(jsonFile)
                jsonFile.close()
        else:
            with open(path_to_labels) as f:
                labels = list()
                for line in f:
                    labels.append(line.replace("\n", ""))

        return len(labels), labels


    k = 47
    for images in os.listdir(folder_dir):
        k = k + 1
        if (images.endswith(".jpg")):
            for models_name in settings.models_names:
                len_labels, labels = open_labels('coco_labels.json')
                model, parameter_dict = settings.take_a_model(models_name, device, labels)
                safety = SafetyAlgorithm(model)

                # TBD opencv image read
                img = cv2.imread(folder_dir+"image"+str(k)+".jpg")
                im_1_orignal = np.asarray(Image.open(folder_dir+"image"+str(k)+".jpg"))
                im_1 = np.moveaxis(im_1_orignal/255.0, -1, 0)
                im_1 = np.expand_dims(im_1, axis=0)
                im_1 = torch.Tensor(np.asarray(im_1)).to(device)

                image = preprocess_vid.read_json_frame("../annotated_images/image" + str(k)+".json")

                boxes, classes, labels, scores, gts = list(), list(), list(), list(), list()
                for entries in image["shapes"]:
                    label = entries["label"]
                    points = entries["points"]
                    gts.append([label, points])


                #x = [torch.rand(3, 320, 320), torch.rand(3, 500, 400)]
                with torch.no_grad():

                    model.predict(im_1, 0.5, boxes, classes, labels, scores)

                    for box, cls, label, score, gt in zip(boxes, classes, labels, scores, gts):
                        # TBD - change the function "draw_boxes"
                        labeled_image = functions.draw_box(box,
                                                           cls,
                                                           label,
                                                           im_1_orignal,
                                                           line=1)

                    cv2.imwrite("C:/Users/benchikha/predictions/image" + str(k) + '.png',labeled_image)


            # TBD - calculate average precision, confusion matrix
            # look in the utils - metrics
