import cv2
import os
from os.path import isfile, join
import json
import numpy as np
from os.path import isfile, join


class PreprocessVideo:
    def __init__(self, video_path):
        self.video_path = video_path

    def read_json_frame(self, path_to_jonson):
        with open(path_to_jonson) as f:
            d = json.load(f)
            return d

    def get_num_frames(self):
        cap = cv2.VideoCapture(self.video_path)
        total_frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        fps = cap.get(cv2.CAP_PROP_FPS)
        print("The total number of frames in this video is ", total_frame_count,
              "//number of frames per second", fps, "//Video Duration", total_frame_count/fps)
        return fps, total_frame_count


    def get_all_frames(self, path_to_save_frame):
        i = 0
        cap = cv2.VideoCapture(self.video_path)
        frame_count = 0

        while (cap.isOpened()):
            ret, frame = cap.read()
            # This condition prevents from infinite looping
            # incase video ends.
            # @FixMe here put the condition to break after pressing the CTRL+Q
            if ret == False:
                break
            # Save Frame by Frame into disk using imwrite method

            cv2.imwrite(path_to_save_frame + str(frame_count) + '.jpg', frame)
            frame_count += 1
            i += 1
            if cv2.waitKey(25) & 0xFF == ord('q'):
                break


    def get_frames_from_video(self, path_to_save_frame, num_of_frames_to_skip):
        cap = cv2.VideoCapture(self.video_path)
        i = 0
        # a variable to keep track of the frame to be saved
        frame_count = 0
        # @FixMe what is the i good for?
        while cap.isOpened():
            ret, frame = cap.read()
            if not ret:
                break
            if i > num_of_frames_to_skip - 1:
                frame_count += 1
                cv2.imwrite(path_to_save_frame + str(frame_count * num_of_frames_to_skip) + '.jpg', frame)
                i = 0
                continue
            i += 1

    def get_video_from_frames(self, path_input, path_output, fps):
            frame_array = []
            files = [f for f in os.listdir(path_input) if isfile(join(path_input, f))]
            # for sorting the file names properly
            files.sort(key=lambda x: int(x[5:-4]))
            # @FixMe this should work as well
            size = (1300, 900)
            out = cv2.VideoWriter(path_output, cv2.VideoWriter_fourcc(*'DIVX'), fps, size)
            for i in range(len(files)):
                filename = path_input + files[i]
                # reading each files
                img = cv2.imread(filename)
                height, width, layers = img.shape
                size = (width, height)
                # @FixMe Writing the image within the loop is probably faster thatn to store them and then write them
                out.write(img)
                # inserting the frames into an image array
                frame_array.append(img)
                out.write(frame_array[i])

            out.release()

    def eval_obj_det_per_frame(self, evaluated_images_m1, evaluated_image_m2,
                               evaluation_m1, evaluation_m2, model_name_1, model_name_2):

        # @FixMe Please make the size of the video dependent based on the input video size
        size = (1500, 900)

        vertical_concat = np.concatenate((evaluated_images_m1, evaluated_image_m2), axis=0)
        blank_image = np.zeros(shape=vertical_concat.shape, dtype=np.uint8)

        cv2.putText(blank_image,
                    'Statistic for class person',
                    (250, 100),
                    cv2.FONT_HERSHEY_SIMPLEX, 4,
                    (255, 255, 255),
                    5,
                    cv2.LINE_4)
        models = [model_name_1, model_name_2]
        evalaution_metrics = ['Average precision:', 'True positive:', 'False positive:', 'Precision:'
                              , 'Recall:']
        distance_titles = 0
        distance_eval_metrics = 0
        evaluation = {model_name_1: evaluation_m1, model_name_2: evaluation_m2}
        for model in models:

            cv2.putText(blank_image, model + ":", (100, 250 + distance_titles),
                        cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 0, 255), 5, cv2.LINE_4)
            eval_index = 0
            shift = 0
            if model == model_name_2:
                shift = 150
            for text in evalaution_metrics:

                distance_eval_metrics = 150 + distance_eval_metrics
                cv2.putText(blank_image, text + str(round(evaluation[model][eval_index], 2)),
                            (100, 250 + distance_eval_metrics+shift),
                            cv2.FONT_HERSHEY_SIMPLEX, 2,
                            (0, 255, 255),
                            5,
                            cv2.LINE_4)
                eval_index = eval_index + 1
            distance_titles = 900

            # add labels for statistic

        horizontal_concat = np.concatenate((vertical_concat, blank_image), axis=1)
        # dsize
        output_image = cv2.resize(horizontal_concat, size)
        cv2.imshow('frame', output_image)

    def eval_obj_det(self, list_of_evaluated_images_m1, list_of_evaluated_image_m2,
                     list_of_evaluation_m1, list_of_evaluation_m2, model_name_1, model_name_2, fps,
                     path_to_save_video):

        list_of_concat_images = list()
        # @FixMe Please make the size of the video depandent based on the input video size
        size = (1300, 900)
        video = cv2.VideoWriter(path_to_save_video, cv2.VideoWriter_fourcc(*'DIVX'), fps, size)

        for image_m1, image_m2, evaluation_m1, evaluation_m2 in zip(list_of_evaluated_images_m1,
                                                                    list_of_evaluated_image_m2,
                                                                    list_of_evaluation_m1,
                                                                    list_of_evaluation_m2):

            vertical_concat = np.concatenate((image_m1, image_m2), axis=0)
            blank_image = np.zeros(shape=vertical_concat.shape, dtype=np.uint8)
            # add title
            # @FixMe Please define as global variable all the sizes which you are using
            # @FixMe such as: first label (250, 100) etc... and ude the variable instead
            # @FixMe If you put all he positions in list you can iterate within for loop and avoid repeating the putText

            cv2.putText(blank_image,
                        'Statistic for class person',
                        (250, 100),
                        cv2.FONT_HERSHEY_SIMPLEX, 4,
                        (255, 255, 255),
                        5,
                        cv2.LINE_4)
            models = [model_name_1, model_name_2]
            evalaution_metrics = ['Average precision:', 'True positive:', 'False positive:', 'Precision:'
                                  , 'Recall:']
            distance_titles = 0
            distance_eval_metrics = 0

            evaluation = {model_name_1: evaluation_m1, model_name_2: evaluation_m2}
            for model in models:
                cv2.putText(blank_image, model + ":", (100, 250 + distance_titles),
                            cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 0, 255), 5, cv2.LINE_4)
                eval_index = 0

                for text in evalaution_metrics:
                    # add model name
                    eval_index = eval_index + 1
                    cv2.putText(blank_image, text + str(round(evaluation[model][eval_index], 2)),
                                (100, 250 + distance_eval_metrics + distance_titles),
                                cv2.FONT_HERSHEY_SIMPLEX, 2,
                                (0, 255, 255),
                                5,
                                cv2.LINE_4)
                    distance_eval_metrics = 150
                distance_titles = 900
                # add labels for statistic

            horizontal_concat = np.concatenate((vertical_concat, blank_image), axis=1)
            # dsize
            output_image = cv2.resize(horizontal_concat, size)
            list_of_concat_images.append(output_image)
            # video.write(output_image)

        return list_of_concat_images













