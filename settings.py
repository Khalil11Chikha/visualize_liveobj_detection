import os
import collections
import torch
import torchvision
import json

import scripts.dataset as DataSet
import scripts.model as ModelWrapper
import scripts.detr_wrapper as Detr
from scripts.visualization import Visualization

logging_images = False
batch_size = 16

path_to_logging_file = os.path.join("data", "logging")
path_to_labels = os.path.join("data", "dataset", "labels", "coco_labels.json")
path_to_data = os.path.join("data", "dataset", "coco_dataset", "val2017")
path_to_annotations = os.path.join("data", "dataset", "coco_dataset", "annotations", "instances_val2017.json")

#all_models_names = ["ssdlite320_mobilenet_v3_large", "fasterrcnn_resnet50_fpn", "yolov5", "detr"]


models_names = ["fasterrcnn_resnet50_fpn", "ssdlite320_mobilenet_v3_large"]

list_of_analyzed_layers = None
images_to_be_investigated = [5, 7, 11, 30, 42, 64, 69, 81, 86, 89, 94, 108, 114, 134, 164, 197, 207]
images_to_be_investigated = ["all"]

images_to_be_investigated = [int(index) for index in range(0, 128, 1)]

global_analyzed_classes = ["person", "car", "bicycle", "motorcycle", "bus", "train",
                           "truck", "traffic light", "street sign", "stop sign"]
global_analyzed_classes = ["person"]

allowed_classes = None


def custom_layers_and_kernels(logging_path, most_or_anti_neurons, number_of_analyzed_neurons):
    with open(os.path.join(logging_path, "criticality", most_or_anti_neurons + '_critical_neurons.json'),
              'r') as json_file:
        critical_neurons = json.load(json_file)
    sorted_critical_neurons = Visualization.filter_neurons(critical_neurons)

    def nested_dict():
        return collections.defaultdict(nested_dict)

    ciritical_neurons_dict = nested_dict()
    count = 0
    for mean_value, std_values in sorted_critical_neurons.items():
        for std_value, labels in std_values.items():
            for label, critical_neuron in labels.items():
                if 0.5 > std_value > 0.01 and label == global_analyzed_classes and count < number_of_analyzed_neurons:
                    ciritical_neurons_dict[critical_neuron["layer"]][critical_neuron["kernel"]] = std_value
                    count += 1

    return ciritical_neurons_dict


def return_weights(_model, _models_name):
    _parameter_dict = collections.defaultdict(list)
    if _models_name == "detr":
        models = [_model.model.backbone, _model.model.transformer]
    else:
        models = [_model]

    for pytorch_model in models:
        for name, parameter in pytorch_model.named_parameters():
            if list_of_analyzed_layers is not None:
                # TBD add biases and mask them simultaniusly
                if 'weight' in name and name in list_of_analyzed_layers:
                    _parameter_dict[name].append(parameter)
            else:
                if 'weight' in name:
                    _parameter_dict[name].append(parameter)

    return _parameter_dict


def take_a_model(name, device, labels):
    if name == "fasterrcnn_resnet50_fpn":
        _model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=True,
                                                                      progress=True,
                                                                      num_classes=91,
                                                                      trainable_backbone_layers=False)
        _parameter_dict = return_weights(_model, name)

        _mapped_model = ModelWrapper.CNNWrapper(_model, device, name, labels)

    elif name == "ssdlite320_mobilenet_v3_large":
        _model = torchvision.models.detection.ssdlite320_mobilenet_v3_large(pretrained=True,
                                                                            progress=True,
                                                                            num_classes=91,
                                                                            trainable_backbone_layers=False)
        _parameter_dict = return_weights(_model, name)

        _mapped_model = ModelWrapper.CNNWrapper(_model, device, name, labels)

    elif name == "yolov5":
        # https://pytorch.org/hub/ultralytics_yolov5/
        # pip install -qr https://raw.githubusercontent.com/ultralytics/yolov5/master/requirements.txt
        _model = torch.hub.load('ultralytics/yolov5', 'yolov5l', pretrained=True)  # large version taken
        _model.conf = 0.5  # NMS confidence threshold
        _model.iou = 0.5  # NMS IoU threshold
        _model.agnostic = False  # NMS class-agnostic
        _model.multi_label = False  # NMS multiple labels per box
        _model.classes = None  # (optional list) filter by class, i.e. = [0, 15, 16] for COCO persons, cats and dogs
        _model.max_det = 300  # maximum number of detections per image
        _model.amp = False  # Automatic Mixed Precision (AMP) inference
        #_model = torch.hub.load('ultralytics/yolov5', 'custom', 'yolov5s.mlmodel')
        #   torch:           = torch.zeros(16,3,320,640)  # BCHW (scaled to size=640, 0-1 values)
        _parameter_dict = return_weights(_model, name)
        _mapped_model = ModelWrapper.CNNWrapper(_model, device, name, labels)

    elif name == "detr":
        # https://colab.research.google.com/github/facebookresearch/detr/blob/colab/notebooks/detr_demo.ipynb
        _model = torch.hub.load('facebookresearch/detr', 'detr_resnet50', pretrained=True, num_classes=91)
        _model.eval()
        _model.to(device)

        #_mapped_model = Detr.DETRWrapper(_model, _dataset)
        #_parameter_dict = return_weights(_mapped_model, name)

    else:
        raise ValueError("Wrong models name.")

    print("{}, is being analyzed.".format(name))
    print("{}, layers is being analyzed.".format(len(list(_parameter_dict.keys()))))

    return _mapped_model, _parameter_dict
