# Visualuastion tool for detection results

Code to evaluate and visualise the prediction results of different object detection modules.

# Requirements:

1- Labelme tool to label the frames of the video.
2- pretained models.

# Labelme:

you can install and find all details of the tool Labelme in this link: https://github.com/CSAILVision/LabelMeAnnotationTool
During labeling, you have to save the frames in "annotated images" folder. They will be needed to be the input of this code. 
You can use the function get_frames_from_video() to obtain your frames form the video. 

# Pretrained models:

The code evaluates and visualises the detection results of two modules simultaniously. 
The modules are given in the settings.py scrpit in a list called models_names.


 
